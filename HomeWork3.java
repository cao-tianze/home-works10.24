package com.powernode.homeworks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


class DateUtil{
    /**
     * 用来实现字符串转换成Date
     */
    public static Date str2Date(String s) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(s);
    }

    /**
     * Date转换成字符串
     */
    public static String date2Str(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
public class HomeWork3 {
    public static void main(String[] args) throws ParseException {
        String s = "2017-10-20";
        System.out.println(DateUtil.str2Date(s));
        Date date = new Date();
        System.out.println(DateUtil.date2Str(date));
    }
}
