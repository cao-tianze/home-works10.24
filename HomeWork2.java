package com.powernode.homeworks;


interface Printer{
    void print();
}
class DoMatriPrinter implements Printer{
    @Override
    public void print() {
        System.out.println("打印速度慢,效果差,噪音高");
    }
}
class InkperPrinter implements Printer{
    @Override
    public void print() {
        System.out.println("打印效果介于针式和激光打印机之间");
    }
}
class LaserPrinter implements Printer{
    @Override
    public void print() {
        System.out.println("打印速度快,效果好,噪音小");
    }
}
public class HomeWork2 {
    public static void main(String[] args) {
        new DoMatriPrinter().print();
        new InkperPrinter().print();
        new LaserPrinter().print();
    }
}
