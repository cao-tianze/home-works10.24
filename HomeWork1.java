package com.powernode.homeworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;


public class HomeWork1 {
    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader("E:\\course\\students\\20.lambda表达式与stream流\\code\\java_01\\1.txt");
        FileWriter fw = new FileWriter("E:\\course\\students\\20.lambda表达式与stream流\\code\\java_01\\2.txt");
        int len = -1;
        char[] ch = new char[1024];
        if ((len = fr.read(ch))!=-1){
            fw.write(ch,0,len);
        }
        fw.close();
        fr.close();
        BufferedReader br = new BufferedReader(new FileReader("E:\\course\\students\\20.lambda表达式与stream流\\code\\java_01\\1.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("E:\\course\\students\\20.lambda表达式与stream流\\code\\java_01\\3.txt"));
        String line =null;
        int count = 0;
        while ((line = br.readLine())!=null){
            if (line.contains("a")){
                System.out.println(line);
                count++;
            }
            bw.write(line);
            bw.newLine();
        }
        bw.close();
        br.close();
        System.out.println("一共有"+count+"个");
    }
}
