package com.powernode.homeworks;

import java.util.Scanner;

class UserService{
    public static void login(){
        for (int i = 1; i <=3; i++) {
            //键盘录入数据
            Scanner input = new Scanner(System.in);
            System.out.print("请输入用户名：");
            String username = input.next();
            System.out.print("请输入密码：");
            String userPassword = input.next();
            if ("admin".equals(username) && "admin".equals(userPassword)){
                System.out.println("欢迎admin登录");
                return;
            }
            if (i<3) {
                System.out.println("用户名或密码错误,您还有"+(3-i)+"次机会");
                continue;
            }
            System.out.println("用户名或密码错误!今日机会已用完,请明天再试!");
        }
    }
}
public class HomeWork4 {
    public static void main(String[] args) {
        UserService.login();
    }
}
